
DOCKER := $(shell command -v docker 2> /dev/null)
DOCKER_COMPOSE := $(shell command -v docker-compose 2> /dev/null)
CPU := $(shell dpkg --print-architecture)
LSB_RELEASE := $(shell lsb_release -cs)
TYPE := $(shell uname -s)
ARCH := $(shell uname -m)
DOCKER_PS := $(shell docker ps -a -q)
VM_IP := $(shell hostname -I | awk '{print $$1}')


install_docker:
	@echo "\e[1m\e[32m-- DOCKER Installation --\e[22m\e[24m\e[39m "
	@echo "Checking if the resources are installed ... "
ifndef DOCKER
	@echo "DOCKER \e[1m\e[31m FAIL \e[22m\e[24m\e[39m "
	@echo "The installation will start in 5 seconds. Ctrl+C to cancel ... "
	@sleep 5
	@sudo apt-get update
	@sudo apt-get -y install ca-certificates curl gnupg lsb-release
	@curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg 
	@echo "deb [arch=$(CPU) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(LSB_RELEASE) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	@sudo apt-get update
	@sudo apt-get -y install docker-ce docker-ce-cli containerd.io
	@echo "Installation completed "
	@docker --version
else
	@echo "DOCKER \e[1m\e[32m OK \e[22m\e[24m\e[39m "
	@docker --version	
endif

install_docker-compose:
	@echo "\e[1m\e[32m-- DOCKER-COMPOSE Installation --\e[22m\e[24m\e[39m "
	@echo "Checking if the resources are installed ... "
ifndef DOCKER_COMPOSE
	@echo "DOCKER-COMPOSE \e[1m\e[31m FAIL \e[22m\e[24m\e[39m"
	@echo "The installation will start in 5 seconds. Ctrl+C to cancel ... "
	@sleep 5
	@sudo apt-get update
	@sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(TYPE)-$(ARCH)" -o /usr/local/bin/docker-compose
	@sudo chmod +x /usr/local/bin/docker-compose
	@echo "Installation completed "
	@docker-compose --version
else
	@echo "DOCKER-COMPOSE \e[1m\e[32m OK \e[22m\e[24m\e[39m"
	@docker-compose --version
endif

create_ssl:
	@echo "\e[1m\e[32m-- SSL certificate creation --\e[22m\e[24m\e[39m "
	@echo "Creating directory structure for CA"
	@mkdir -p vaultwarden/data/ssl/certs
	@mkdir -p vaultwarden/data/ssl/csr
	@mkdir -p vaultwarden/data/ssl/private
	@mkdir -p vaultwarden/data/ssl/newcerts
	@touch vaultwarden/data/ssl/index.txt
	@echo 1000 > vaultwarden/data/ssl/serial
	@echo "Creating key for personal CA, please fill out the FQDN with the name you want for your CA"
	@sudo openssl genrsa -aes256 -out vaultwarden/data/ssl/private/myCA.key 4096
	@sudo chmod 500 vaultwarden/data/ssl/private/myCA.key
	@echo ""
	@echo "Creating personal CA"
	@sudo openssl req -config vaultwarden/data/ssl/bitwarden.ext -x509 -new -sha256 -days 3650 -extensions v3_ca -key vaultwarden/data/ssl/private/myCA.key -out vaultwarden/data/ssl/certs/myCA.crt
	@echo ""
	@echo "Creating key for bitwarden certificate"
	@sudo openssl genrsa -out vaultwarden/data/ssl/private/bitwarden.key 2048
	@echo ""
	@echo "Creating request for Bitwarden certificate, please fill out the FQDN with the name that the instance will be located at"
	@sudo openssl req -config vaultwarden/data/ssl/bitwarden.ext -key vaultwarden/data/ssl/private/bitwarden.key -new -sha256 -out vaultwarden/data/ssl/csr/bitwarden.csr
	@echo ""
	@echo -n "Please enter your FQDN for your bitwarden instance: "
	@read answer
	@sed -i "/DNS.1 = */c\DNS.1 = $answer" ./vaultwarden/data/ssl/bitwarden.ext
	@sed -i "/DNS.2 = */c\DNS.2 = www.$answer" ./vaultwarden/data/ssl/bitwarden.ext
	@sudo openssl ca -config vaultwarden/data/ssl/bitwarden.ext -extensions server_cert -days 375 -notext -md sha256 -in vaultwarden/data/ssl/csr/bitwarden.csr -out vaultwarden/data/ssl/certs/bitwarden.crt
	@echo ""
	@echo "Your personal CA has been created, please make sure to install myCA.crt as a trusted root CA in all devices you want to connect to this instance"

install: install_docker install_docker-compose

ingress_setup:
	@echo "\e[1m\e[32m-- Mounting INGRESS containers --\e[22m\e[24m\e[39m"
	@sleep 3
	docker-compose -f ingress/ingress.yaml up -d	

ingress_clean:
	@echo "\e[1m\e[32m-- Removing INGRESS containers --\e[22m\e[24m\e[39m"
	@sleep 2
	docker-compose -f ingress/ingress.yaml down --remove-orphans	

zabbix_setup:
	@echo "\e[1m\e[32m-- Mounting ZABBIX containers --\e[22m\e[24m\e[39m"
	@sleep 3
	docker-compose -f zabbix/zabbix_ubuntu_mysql_nginx.yaml up -d
	@docker exec -ti zabbix-server apk update
	@docker exec -ti zabbix-server apk upgrade
	@docker exec -ti zabbix-server apk add sudo
	@docker exec -ti zabbix-server apk add nmap
	@docker exec -ti zabbix-server apk add nano
#	@docker exec -ti --user root zabbix-server echo "zabbix  ALL=(root) NOPASSWD: /usr/bin/nmap" | sudo tee -a /etc/sudoers >/dev/null
#	@docker exec -ti --user root zabbix-server echo "zabbix  ALL=(root) NOPASSWD: /usr/bin/traceroute" | sudo tee -a /etc/sudoers >/dev/null

zabbix_clean:
	@echo "\e[1m\e[32m-- Removing ZABBIX containers --\e[22m\e[24m\e[39m"
	@sleep 2
	docker-compose -f zabbix/zabbix_ubuntu_mysql_nginx.yaml down --remove-orphans	

bitwarden_setup:
	@echo "\e[1m\e[32m-- Mounting BITWARDEN containers --\e[22m\e[24m\e[39m"
	@sleep 3
	docker-compose -f vaultwarden/bitwarden_nginx.yaml up -d

bitwarden_clean:
	@echo "\e[1m\e[32m-- Removing BITWARDEN containers --\e[22m\e[24m\e[39m"
	@sleep 2
	docker-compose -f vaultwarden/bitwarden_nginx.yaml down --remove-orphans

portainer_setup:
	@echo "\e[1m\e[32m-- Mounting PORTAINER containers --\e[22m\e[24m\e[39m"
	@sleep 3
	docker-compose -f portainer/portainer.yaml up -d

portainer_clean:
	@echo "\e[1m\e[32m-- Removing PORTAINER containers --\e[22m\e[24m\e[39m"
	@sleep 2
	docker-compose -f portainer/portainer.yaml down --remove-orphans

container_setup: ingress_setup zabbix_setup bitwarden_setup portainer_setup

container_clean: ingress_clean zabbix_clean bitwarden_clean portainer_clean

restart_docker:
	@echo "\e[1m\e[32m-- Pruning docker images and networks --\e[22m\e[24m\e[39m"
	docker rm -f $(DOCKER_PS)
	docker system prune -a

remove_docker-compose:
	@sudo rm -R /usr/local/bin/docker-compose

simple_host:
	docker exec -ti simple-host bash

check_connectivity:
	@echo "\e[1m\e[32m-- Checking IPv4 connectivity --\e[22m\e[24m\e[39m"
	docker exec -ti simple-host ping -c 3 192.168.10.1;
	@echo ""
	docker exec -ti simple-host ping -c 3 192.168.10.5;
	@echo ""
	docker exec -ti simple-host ping -c 3 $(VM_IP);

#action:
#	@echo $(filter-out $@,$(MAKECMDGOALS))